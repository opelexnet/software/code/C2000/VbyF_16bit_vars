/*
 * GlobalVariables.h
 *
 * Author    : Najath Abdul Azeez
 * Copyright : http://opelex.net
 * License   : See LICENSE file
 *
 */

#ifndef GLOBALVARIABLES_H_
#define GLOBALVARIABLES_H_

// Frequency and angles
extern Int16   freq, freq_set;
extern Int32   freq_state;
extern Uint32  thetaState;
extern Uint16  theta, theta90;
extern Int16   sinTheta, cosTheta;

// Voltages
extern Int16  Vdc;
extern Int16  Vd, Vq;
extern Int16  Valfa, Vbeta;
extern Int16  Va, Vb, Vc;

// PWM Timing computation
extern Int16  Ta, Tb, Tc;
extern Int16  Tmax, Tmin;
extern Int16  Teff, Tzero, Toffset;
extern Int16  Tao, Tbo, Tco;

// define enum type for PWM technique
typedef enum {SINE, SVPWM, LCLAMP, HCLAMP} pwm_t;
extern pwm_t pwm;

// Sine Table Q15
extern const Int16 sine[1024];

#endif /* GLOBALVARIABLES_H_ */
