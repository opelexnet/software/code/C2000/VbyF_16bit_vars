/*
 * main.c
 *
 * Author    : Najath Abdul Azeez
 * Copyright : http://opelex.net
 * License   : See LICENSE file
 *
 */
#include "F2806x_Device.h"     // Include F2806x Header files
#include "functions.h"         // Include function prototypes

void main(void)
{

    InitSysCtrl();

    InitVars();

    InitGpio();
    InitEPwm();

    InitInterrupts();

    // Enable global Interrupts and higher priority real-time debug events:
    EINT;   // Enable Global interrupt INTM
    // ERTM;   // Enable Global realtime interrupt DBGM

    while(1)
    {
        //
    }

}

