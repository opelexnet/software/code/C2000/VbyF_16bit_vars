/*
 * Epwm.c
 *
 * Author    : Najath Abdul Azeez
 * Copyright : http://opelex.net
 * License   : See LICENSE file
 *
 */
//
// TITLE:   EPWM Initialization & Support Functions.
//

#include "F2806x_Device.h"     // Include F2806x Header files
#include "functions.h"         // Include function prototypes
#include "macros.h"            // Include macro definitions

void InitEPwm(void)
{

    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;      // Stop all the TB clocks
    EDIS;

    // Epwm 1
    // Timer Base
    EPwm1Regs.TBCTL.all = 0xC033;
    EPwm1Regs.TBPRD = PERIOD;
    EPwm1Regs.TBPHS.half.TBPHS = 0x0000;
    EPwm1Regs.TBCTR = 0x0000;

    // Compare Control
    EPwm1Regs.CMPCTL.all = 0x0000;
    EPwm1Regs.CMPA.half.CMPA = 0x6000;
    EPwm1Regs.CMPB = 0x3000;

    // Action Qualifier
#if RAMP
    EPwm1Regs.AQCTLA.bit.ZRO = 2; // Set on Zero
#else  // not RAMP, Traingular carrier
    EPwm1Regs.AQCTLA.bit.CAD = 2; // Set on Compare A down count match
#endif // RAMP
    EPwm1Regs.AQCTLA.bit.CAU = 1; // Clear on Compare A match
    EPwm1Regs.AQCTLB.bit.ZRO = 2; // Set on Zero
    EPwm1Regs.AQCTLB.bit.CBU = 1; // Clear on Compare B match

    // Dead Band Control
    EPwm1Regs.DBCTL.all = 0x000B; // Enable both dead bands
    EPwm1Regs.DBRED     = 0x0050; // 50 clock cycle Delay
    EPwm1Regs.DBFED     = 0x0050; // 50 clock cycle Delay

    // Event Trigger
    EPwm1Regs.ETSEL.bit.INTSEL = 1;
    EPwm1Regs.ETSEL.bit.INTEN  = 1;
    EPwm1Regs.ETPS.bit.INTPRD  = 1; //Interrupt every event

    EPwm1Regs.TBCTL.bit.CTRMODE = 2; // Up-down Count mode

    // Epwm 2
    // Timer Base
    EPwm2Regs.TBCTL.all = 0xC033;
    EPwm2Regs.TBPRD = PERIOD;
    EPwm2Regs.TBPHS.half.TBPHS = 0x0000;
    EPwm2Regs.TBCTR = 0x0000;

    // Compare Control
    EPwm2Regs.CMPCTL.all = 0x0000;
    EPwm2Regs.CMPA.half.CMPA = 0x6000;
    EPwm2Regs.CMPB = 0x3000;

    // Action Qualifier
#if RAMP
    EPwm2Regs.AQCTLA.bit.ZRO = 2; // Set on Zero
#else  // not RAMP, Traingular carrier
    EPwm2Regs.AQCTLA.bit.CAD = 2; // Set on Compare A down count match
#endif // RAMP
    EPwm2Regs.AQCTLA.bit.CAU = 1; // Clear on Compare A match
    EPwm2Regs.AQCTLB.bit.ZRO = 2; // Set on Zero
    EPwm2Regs.AQCTLB.bit.CBU = 1; // Clear on Compare B match

    // Dead Band Control
    EPwm2Regs.DBCTL.all = 0x000B; // Enable both dead bands
    EPwm2Regs.DBRED     = 0x0050; // 50 clock cycle Delay
    EPwm2Regs.DBFED     = 0x0050; // 50 clock cycle Delay

    // Event Trigger
//    EPwm2Regs.ETSEL.bit.INTSEL = 1;
//    EPwm2Regs.ETSEL.bit.INTEN  = 1;
//    EPwm2Regs.ETPS.bit.INTPRD  = 1; //Interrupt every event

    EPwm2Regs.TBCTL.bit.CTRMODE = 2; // Up-down Count mode

    // Epwm 3
    // Timer Base
    EPwm3Regs.TBCTL.all = 0xC033;
    EPwm3Regs.TBPRD = PERIOD;
    EPwm3Regs.TBPHS.half.TBPHS = 0x0000;
    EPwm3Regs.TBCTR = 0x0000;

    // Compare Control
    EPwm3Regs.CMPCTL.all = 0x0000;
    EPwm3Regs.CMPA.half.CMPA = 0x6000;
    EPwm3Regs.CMPB = 0x3000;

    // Action Qualifier
#if RAMP
    EPwm3Regs.AQCTLA.bit.ZRO = 2; // Set on Zero
#else  // not RAMP, Traingular carrier
    EPwm3Regs.AQCTLA.bit.CAD = 2; // Set on Compare A down count match
#endif // RAMP
    EPwm3Regs.AQCTLA.bit.CAU = 1; // Clear on Compare A match
    EPwm3Regs.AQCTLB.bit.ZRO = 2; // Set on Zero
    EPwm3Regs.AQCTLB.bit.CBU = 1; // Clear on Compare B match

    // Dead Band Control
    EPwm3Regs.DBCTL.all = 0x000B; // Enable both dead bands
    EPwm3Regs.DBRED     = 0x0050; // 50 clock cycle Delay
    EPwm3Regs.DBFED     = 0x0050; // 50 clock cycle Delay

    // Event Trigger
//    EPwm3Regs.ETSEL.bit.INTSEL = 1;
//    EPwm3Regs.ETSEL.bit.INTEN  = 1;
//    EPwm3Regs.ETPS.bit.INTPRD  = 1; //Interrupt every event

    EPwm3Regs.TBCTL.bit.CTRMODE = 2; // Up-down Count mode

    // Epwm 7
    // Timer Base
    EPwm7Regs.TBCTL.all = 0xC033;
    EPwm7Regs.TBPRD = PERIOD;
    EPwm7Regs.TBPHS.half.TBPHS = 0x0000;
    EPwm7Regs.TBCTR = 0x0000;

    // Compare Control
    EPwm7Regs.CMPCTL.all = 0x0000;
    EPwm7Regs.CMPA.half.CMPA = 0x6000;
    EPwm7Regs.CMPB = 0x3000;

    // Action Qualifier
#if RAMP
    EPwm7Regs.AQCTLA.bit.ZRO = 2; // Set on Zero
#else  // not RAMP, Traingular carrier
    EPwm7Regs.AQCTLA.bit.CAD = 2; // Set on Compare A down count match
#endif // RAMP
    EPwm7Regs.AQCTLA.bit.CAU = 1; // Clear on Compare A match
#if RAMP
    EPwm7Regs.AQCTLB.bit.ZRO = 2; // Set on Zero
#else  // not RAMP, Traingular carrier
    EPwm7Regs.AQCTLB.bit.CBD = 2; // Set on Compare B down count match
#endif // RAMP
    EPwm7Regs.AQCTLB.bit.CBU = 1; // Clear on Compare B match

    // Dead Band Control
    EPwm7Regs.DBCTL.all = 0x0000; // Disable dead band
    EPwm7Regs.DBRED     = 0x0000; // No Delay
    EPwm7Regs.DBFED     = 0x0000; // No Delay

    // Event Trigger
//    EPwm7Regs.ETSEL.bit.INTSEL = 1;
//    EPwm7Regs.ETSEL.bit.INTEN  = 1;
//    EPwm7Regs.ETPS.bit.INTPRD  = 1; //Interrupt every event

    EPwm7Regs.TBCTL.bit.CTRMODE = 2; // Up-down Count mode

    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;         // Start all the timers synced
    EDIS;

}
