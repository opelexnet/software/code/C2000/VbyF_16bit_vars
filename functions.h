/*
 * functions.h
 *
 * Author    : Najath Abdul Azeez
 * Copyright : http://opelex.net
 * License   : See LICENSE file
 *
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

// Include prototypes of Sysctrl functions defined in SysCtrl.c
#include "SysCtrl.h"

// Defined in GlobalVariables.c
void InitVars(void);

// Defined in Gpio.c
void InitGpio(void);

// Defined in Epwm.c
void InitEPwm(void);

// Defined in Interrupt.c
void InitInterrupts(void);

// Defined in ISRs.c
__interrupt void epwm1_ISR(void);
__interrupt void default_ISR(void);
__interrupt void PIE_RESERVED(void);
__interrupt void rsvd_ISR(void);


// Defined in F2806x_usDelay.asm
extern void DSP28x_usDelay(Uint32 Count);

// Macro for using DSP28x_usDelay, with configured CPU clock.
#define DELAY_US(A)  DSP28x_usDelay(((((long double) A * 1000.0L) / (long double)CPU_RATE) - 9.0L) / 5.0L)

// The following pointer to a function call calibrates the ADC and internal oscillators
#define Device_cal (void   (*)(void))0x3D7C80


#endif /* FUNCTIONS_H_ */
